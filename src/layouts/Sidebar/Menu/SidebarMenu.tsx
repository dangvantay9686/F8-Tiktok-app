import classNames from "classnames/bind";
import { ReactNodeArray } from "prop-types";
import { FC } from "react";
import styles from "./SidebarMenu.module.scss";

const $ = classNames.bind(styles);

interface PropTypes {
    children : ReactNodeArray;
}

const SidebarMenu : FC<PropTypes> = ({children} : PropTypes) => {
    return (
        <nav>
            {children}
        </nav>
    )
}

export default SidebarMenu;