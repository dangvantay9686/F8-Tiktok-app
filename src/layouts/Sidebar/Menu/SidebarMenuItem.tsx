import classNames from "classnames/bind";
import { FC, ReactNode, useState } from "react";
import { NavLink } from "react-router-dom";
import styles from "./SidebarMenu.module.scss";

const $ = classNames.bind(styles);

const SidebarMenuItem : FC<PropTypes> = ({icon, title, to} : PropTypes) => {
    
    const [active, setActive] = useState<boolean>(false);

    const handleActive = (navProp : any) : any => {
        setActive(navProp.isActive);
        return $("menu-item", { active : active });
    }

    return (
        <NavLink
            to={to}
            className={(navProp) => handleActive(navProp)}
        >
            {active ? (icon.active) : (icon.regular)}
            <span className={$("menu-item-title")}>{title}</span>
        </NavLink>
    )
}

interface PropTypes {
    icon : {
        regular : ReactNode,
        active : ReactNode
    };
    title : string;
    to : string;
}

export default SidebarMenuItem;