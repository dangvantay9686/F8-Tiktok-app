import { ActiveFollowingIcon, ActiveHomeIcon, ActiveLiveIcon, RegularActiveIcon, RegularFollowingIcon, RegularLiveIcon } from "~/components/Icons";
import config from "~/configs";

export const DataItems: Array<any> = [
    {
        icon: {
            regular: <RegularActiveIcon />,
            active: <ActiveHomeIcon />
        },
        title: "For You",
        to: config.routes.home,
    },

    {
        icon: {
            regular: <RegularFollowingIcon />,
            active: <ActiveFollowingIcon />
        },
        title: "Following",
        to: config.routes.following,
    },

    {
        icon: {
            regular: <RegularLiveIcon />,
            active: <ActiveLiveIcon />
        },
        title: "LIVE",
        to: config.routes.live,
    },
];
