import classNames from "classnames/bind";
import _ from "lodash";
import { FC, useEffect, useState } from "react";
import { User } from "~/components/Models/User";
import { searchByNickName } from "~/layouts/Search";
import { AccountItem } from "./AccountItem";
import styles from "./SidebarAccounts.module.scss";

const $ = classNames.bind(styles);

interface PropsTypes {
    label : string;
    isDisplayInfo : boolean;
}

export const Accounts : FC<PropsTypes> = ({label, isDisplayInfo} : PropsTypes) => {
    
    const [accounts, setAccounts] = useState<Array<User>>([])
    
    useEffect(() => {
        searchByNickName({ q: "hoa", type: "less" })
            .then((res : any) => {
                const userArr = _.map(res.data, (item) => new User(item));
                setAccounts(userArr);
            });
    }, [])
    
    return (
        <div className={$("wrapper")}>

            <p className={$("header-label")}>{ label }</p>

            {accounts.length > 0 && _.map(accounts, (item, i)=> (
                <AccountItem key={i} user={item} isDisplayInfo={isDisplayInfo} />
            ))}

            <p className={$("more-options")}>
                See All
            </p>

        </div>
    );
}