import classNames from "classnames/bind";
import { FC } from "react";

import HeadlessTippy from "@tippyjs/react/headless";

import { faCheckCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import { User } from "~/components/Models/User";
import { PopperWrapper } from "~/components/Popper";
import { AccountPreview } from "../AccountPreview";
import styles from "./AccountInfo.module.scss";

const $ = classNames.bind(styles);

interface PropsTypes {
    user: User;
    isDisplayInfo ?: boolean;
}

export const AccountItem: FC<PropsTypes> = ({ user, isDisplayInfo = false }: PropsTypes) => {
    
    const renderUserInfo = (props : any) : any => {
        return (
            <div className={$("preview")} tabIndex={-1} {...props}>
                <PopperWrapper>
                    {isDisplayInfo && <AccountPreview user={user}/>}
                </PopperWrapper>
            </div>
        );
    };
    
    return (
        <div>
            <HeadlessTippy
                interactive
                delay={[800, 0]}
                offset={[-18, 8]}
                placement="bottom"
                render={renderUserInfo}
            >
                <div className={$("account-item")}>

                    <img className={$("avatar")} src={user.avatar} alt="" />

                    <div className={$("item-info-detail")}>
                        <p className={$("nickname")}>
                            <strong>{user.nickname}</strong>
                            <FontAwesomeIcon className={$("check-box")} icon={faCheckCircle} />
                        </p>

                        <p className={$("full-name")}>{user.fullName}</p>
                    </div>
                </div>
            </HeadlessTippy>
        </div>
    );
}