import { FC } from "react";
import { User } from "~/components/Models/User";

import { faCheckCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import classNames from "classnames/bind";
import Button from "~/components/Button";
import styles from "./AccountPreview.module.scss";

const $ = classNames.bind(styles);

interface PropTypes {
    user: User;
}
export const AccountPreview: FC<PropTypes> = ({ user }: PropTypes) => {
    return (
        <div className={$("wrapper")}>
            <div className={$("header")}>
                <img className={$("avatar")} src={user.avatar} alt={user.fullName} />
                <Button primary>Follow</Button>
            </div>

            <div className={$("body")}>
                <p className={$("nickname")}>
                    <strong>{user.nickname}</strong>
                    <FontAwesomeIcon className={$("check-box")} icon={faCheckCircle} />
                </p>

                <p className={$("full-name")}>{user.fullName}</p>

                <p className={$("analysis")}>
                    <strong className={$("value")}>{user.followersCount + "M "}</strong>

                    <span className={$("label")}>FOLLOWERS</span>

                    <strong className={$("value")}>{user.likesCount}</strong>

                    <span className={$("label")}>LIKES</span>
                </p>
            </div>
        </div>
    );
};
