import classNames from "classnames/bind";
import { MenuItem, SidebarMenu, dataItems } from "./Menu";
import styles from './Sidebar.module.scss';
import { FollowingAccounts, SuggestedAccounts } from "./SidebarAccounts";

const $ = classNames.bind(styles)

export default function Sidebar () {
    return (
        <div className={$('wrapper')}>
            <SidebarMenu>
                {dataItems.map((e : any, index : number) => (
                    <MenuItem key={index} icon={e.icon} title={e.title} to={e.to}/>
                ))}
            </SidebarMenu>

            <SuggestedAccounts label="Suggested accounts" isDisplayInfo/>
            
            <FollowingAccounts label="Following accounts" isDisplayInfo={false}/>
        </div>
    )
}