import classNames from "classnames/bind";
import Header from "~/layouts/Header";
import Sidebar from "~/layouts/Sidebar";
import styles from './DefaultLayout.module.scss';

const $ = classNames.bind(styles)

export default function DefaultLayout ({children} : any) {
    return (
        <div className={$('wrapper')}>
            <Header/>
            <div className={$('container')}>
                <Sidebar/>
                <div className={$('content')}>
                    {children}
                </div>
            </div>
        </div>
    )
};