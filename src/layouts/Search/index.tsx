import _ from "lodash";
import { useEffect, useRef, useState } from "react";

import { faCircleXmark, faSpinner } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import HeadlessTippy from "@tippyjs/react/headless";
import "tippy.js/dist/tippy.css";

import classNames from "classnames/bind";
import styles from "./LayoutSearch.module.scss";

import Account from "~/components/Account";
import { SearchMenuIcon } from "~/components/Icons";
import { User } from "~/components/Models/User";
import { PopperWrapper } from "~/components/Popper";
import { useDebounce } from "~/hooks/useDebounce";
import { request } from "~/utils/http";

const $ = classNames.bind(styles);

export default function Search() {

    const [searchVal, setSearchVal] = useState<any>("");
    const [searchResult, setSearchResult] = useState<Array<User>>([]);
    const [showResult, setShowResult] = useState<boolean>(true);
    const [loading, setLoading] = useState<boolean>(false);

    const inputRef = useRef<any>();

    /**
     * 1 : init = "";
     * 2 : "h" : => call : useDebounce =>  init = ""  && delay => debound = ""
     * 3 : "ho" => call : useDebounce =>  init = ""  && delay => debound = ""
     * 4 : "hoa"  &&  ngừng gõ =>  useDebounce =>  init = ""  && delay => debound = "hoa"
     */
    const debounce = useDebounce(searchVal, 500);

    useEffect(() => {

        if (!debounce.trim()) {
            setSearchResult([]);
            return;
        };

        setLoading(true);

        searchByNickName({ q: debounce, type: "less" })
            .then((res: any) => {
                const users = _.uniqBy(res.data.map((e: any) => new User(e)), (item: User) => item.id);
                setSearchResult(users);
                setLoading(false);
            })
            .catch(() => {
                setLoading(false);
            });

    }, [debounce]);

    const handleHideResult = () => {
        setShowResult(false);
    }

    const handleChange = (e: any) => {
        const searchValue = e.target.value;

        if (!searchValue.startsWith(' ')) {
            setSearchVal(searchValue);
        }
    }

    return (
        //using div, span to avoid warning of type lib
        <div>
            <HeadlessTippy
                interactive
                appendTo={() => document.body}
                visible={showResult && searchResult.length > 0}
                render={(attrs: any) => (
                    <div className={$("search-result")} tabIndex={-1} {...attrs}>
                        <PopperWrapper>
                            <h4 className={$("search-title")}>Accounts</h4>
                            {searchResult.map((user: User) => (
                                <Account key={user.id} user={user} />
                            ))}
                        </PopperWrapper>
                    </div>
                )}
                onClickOutside={handleHideResult}
            >
                <div className={$("search")}>
                    <input
                        ref={inputRef}
                        value={searchVal}
                        placeholder="Search accounts and videos"
                        spellCheck={false}
                        onChange={(e) => handleChange(e)}
                        onFocus={() => setShowResult(true)}
                    />

                    {!!searchVal && !loading && (
                        <button
                            className={$("clear")}
                            onClick={() => {
                                setSearchVal("");
                                inputRef.current.focus();
                            }}
                        >
                            <FontAwesomeIcon icon={faCircleXmark} />
                        </button>
                    )}

                    {loading && <FontAwesomeIcon className={$("loading")} icon={faSpinner} />}

                    <button className={$("search-btn")} onMouseDown={e => e.preventDefault}>
                        <SearchMenuIcon />
                        {/* <FontAwesomeIcon icon={faMagnifyingGlass} /> */}
                    </button>
                </div>
            </HeadlessTippy>
        </div>

    );
}


// API with Fetch:
export function searchQuery(p: any, type: any): Promise<Response> {
    return fetch(`https://tiktok.fullstack.edu.vn/api/users/search?q=${encodeURIComponent(p)}&type=${type}`);
}

// API with Axios:
export async function searchByNickName(param: any): Promise<any> {
    return await request.get("users/search", param);
}
