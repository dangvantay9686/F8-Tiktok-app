import {
    faEllipsisVertical
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Tippy from "@tippyjs/react";
import classNames from "classnames/bind";
import { Link } from "react-router-dom";
import "tippy.js/dist/tippy.css";
import images from "~/assets/imgs";
import Button from "~/components/Button";
import { UploadIcon } from "~/components/Icons";
import { Image } from "~/components/Images";
import { MENU_ITEMS, USER_MENU_ITEMS } from "~/components/Models";
import { PopperMenu } from "~/components/Popper";
import { routePaths } from "~/configs/routes";
import Search from "../Search";
import styles from "./LayoutHeader.module.scss";

const $ = classNames.bind(styles);

export default function Header() {

    //const content = "Tìm kiếm";

    const handleMenuChange = (menuItem: any) => {
        console.log(menuItem);
    };

    const currentUser: boolean = true;

    return (
        <header className={$("wrapper")}>
            <div className={$("inner")}>
                
                <Link to={routePaths.home} className={$("logo")}>
                    <img src={images.logo} alt="TikTok" />
                </Link>

                {/* Seach Coponent: */}
                <Search/>

                <div className={$("action")}>
                    {currentUser ? (
                        <>
                            <div className={$("current-user")}>
                                <Tippy
                                    delay={[0, 200]}
                                    content="Upload video"
                                    placement="bottom"
                                >
                                    <button className={$("action-btn")}>
                                        {/* <FontAwesomeIcon icon={faCloudUpload} /> */}
                                        <UploadIcon />
                                    </button>
                                </Tippy>
                                
                            </div>
                        </>
                    ) : (
                        <>
                            <Button text="true">Upload</Button>

                            <Button primary="true">Log in</Button>
                        </>
                    )}

                    <PopperMenu
                        items={currentUser ? USER_MENU_ITEMS() : MENU_ITEMS()}
                        onChange={handleMenuChange}
                    >
                        {currentUser ? (
                            <Image
                                className={$("user-avatar")}
                                src={images.avatar}
                                alt="hoa"
                                //fallback={images.logo}
                            />
                        ) : (
                            <button className={$("more-btn")}>
                                <FontAwesomeIcon icon={faEllipsisVertical} />
                            </button>
                        )}
                    </PopperMenu>
                </div>
            </div>
        </header>
    );
};
