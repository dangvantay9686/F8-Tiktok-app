import { routePaths as routes } from "./routes";

const config = {
    routes
}

export default config;