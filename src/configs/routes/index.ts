export const routePaths = {
    home: "/",
    following: "/following",
    profile: "/:nickname",
    upload: "/upload",
    search: "/search",
    live : "/live"
}