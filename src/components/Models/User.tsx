export class User {
    public id: any;
    public firstName: any;
    public lastName: any;
    public fullName: any;
    public nickname: any;
    public avatar: any;
    public bio: any;
    public tick: any;
    public followingsCount: any;
    public followersCount: any;
    public likesCount: any;
    public websiteUrl: any;
    public facebookUrl: any;
    public youtubeUrl: any;
    public twitterUrl: any;
    public instagramUrl: any;
    public createdAt: any;
    public updatedAt: any;

    constructor(init: any) {
        this.id = init.id;
        this.firstName = init.first_name;
        this.lastName = init.last_name;
        this.fullName = init.full_name;
        this.nickname = init.nickname;
        this.avatar = init.avatar;
        this.bio = init.bio;
        this.tick = init.tick;
        this.followingsCount = init.followings_count;
        this.followersCount = init.followers_count;
        this.likesCount = init.likes_count;
        this.websiteUrl = init.website_url;
        this.facebookUrl = init.facebook_url;
        this.youtubeUrl = init.youtube_url;
        this.twitterUrl = init.twitter_url;
        this.instagramUrl = init.instagram_url;
        this.createdAt = init.created_at;
        this.updatedAt = init.updated_at;
    }
}
