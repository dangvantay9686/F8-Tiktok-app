import { faCircleQuestion, faCoins, faEarthAsia, faGear, faKeyboard, faSignOut, faUser } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export interface Item {
    icon: any;
    title: any;
    args: any;
}

export const MENU_ITEMS = (): Array<any> => {
    return [
        {
            icon: <FontAwesomeIcon icon={faEarthAsia} />,
            title: "English",
            args: {},
            children: {
                title: "Language",
                data: [
                    {
                        code: "en",
                        title: "English",
                    },
                    {
                        code: "vi",
                        title: "Vietnam",
                    },
                ],
            },
        },
        {
            icon: <FontAwesomeIcon icon={faCircleQuestion} />,
            title: "Feedback and help",
            args: {
                to: "/feedback",
            },
        },
        {
            icon: <FontAwesomeIcon icon={faKeyboard} />,
            title: "Keyboard shortcuts",
            args: {},
        },
    ]
};

export const USER_MENU_ITEMS = (): Array<any> => {
    return [
        {
            icon: <FontAwesomeIcon icon={faUser} />,
            title: "View profile",
            args: {
                to: "/@hoa",
            },
        },
        {
            icon: <FontAwesomeIcon icon={faCoins} />,
            title: "Get coins",
            args: {
                to: "/coin",
            },
        },
        {
            icon: <FontAwesomeIcon icon={faGear} />,
            title: "Setting",
            args: {
                to: "/setting",
            },
        },
        ...MENU_ITEMS(),
        {
            icon: <FontAwesomeIcon icon={faSignOut} />,
            title: "Log out",
            args: {
                to: "/logout",
                separate: true
            },
        },
    ]
};