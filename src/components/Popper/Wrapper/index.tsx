import classNames from "classnames/bind";
import styles from './PopperWrapper.module.scss';

const $ = classNames.bind(styles);

export default function Wrapper({ children, classcustom }: any) {
    return (
        <div className={$('wrapper', classcustom)}>{children}</div>
    )
}

