import classNames from "classnames/bind";
import { FC, useState } from "react";
import styles from "./PopperMenu.module.scss";

import HeadlessTippy from '@tippyjs/react/headless';
import 'tippy.js/dist/tippy.css';
import { PopperWrapper } from "..";

import Button from "~/components/Button";
import MenuHeader from "./Header";

const $ = classNames.bind(styles);

export default function Menu({ children, items= [], onChange = () => {} , hideOnClick = false}: any) {

    const itemArr: Array<any> = items.map((e: any) => e);
    
    const [history, setHistory] = useState<any>([{ data: itemArr }]);

    const current = history[history.length - 1];

    const renderItems = () => {
        return current.data.map((item: any, index: number) => {

            const isParent = !!item.children;

            return <MenuItemElement key={index} item={item} onClick={() => {
                if (isParent) {
                    setHistory((prev: any) => {
                        console.log("push", [...prev, item.children])
                        return [...prev, item.children];
                    })
                } else {
                    onChange(item);
                }
            }} />
        });
    }
    
    return (
        <>
            <HeadlessTippy
                interactive
                delay={[0, 500]}
                offset={[10, 8]}
                hideOnClick={hideOnClick}
                placement="bottom-end"
                render={attrs => (
                    <div className={$('menu-list')} tabIndex={-1} {...attrs}>
                        <PopperWrapper classcustom={$('menu-popper')}>
                            {history.length > 1 && <MenuHeader title="Language" onBack={() => {
                                setHistory((prev : any) => prev.slice(0, prev.length  - 1))
                            }}/>}

                            <div className={$("menu-language-body")}>
                                {renderItems()}
                            </div>
                        </PopperWrapper>
                    </div>
                )}
                onHide={() => setHistory((prev : any) => prev.slice(0, 1))}
            >
                {children}
            </HeadlessTippy>

        </>
    )

}

export const MenuItemElement: FC<any> = ({ item, onClick }) => {
    
    const className = $($('menu-item'), {
        separate : item?.args?.separate
    })
    return (
        <Button
            classcustom={className}
            lefticon={item.icon}
            to={item?.args?.to}
            onClick={onClick}
        >{item.title}</Button>
    );
};
