import { faChevronLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import classNames from "classnames/bind";
import styles from "./MenuHeader.module.scss";

const $ = classNames.bind(styles);

function MenuHeader({ title, onBack }: any) {

    return (
        <>
            <header className={$('header-menu')}>

                <button className={$('back-btn')} onClick={onBack}>
                    <FontAwesomeIcon icon={faChevronLeft}/>
                </button>

                <h4 className={$('header-menu-title')}>
                    {title}
                </h4>

            </header>
        </>
    )

}

export default MenuHeader;