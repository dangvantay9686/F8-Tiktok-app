import classNames from "classnames/bind";
import { Link } from "react-router-dom";
import styles from "./Button.module.scss";

const $ = classNames.bind(styles);

interface Props {
    to: any;
    href: any;

    primary: boolean;
    outline: boolean;
    small: boolean;
    large: boolean;
    text: boolean;
    rounded: boolean;
    disable: boolean;

    classcustom: any;
    children: any;
    orther: any;

    lefticon: any;
    righticon: any;

    onClick: any;
}

export default function Button(init: any) {

    const { to, href, primary, outline, small,
            large, text, rounded, disable, children,
            lefticon, righticon, classcustom }: Props = init;

    let Component: any = "button";

    const classes = $("wrapper", {
        primary,
        outline,
        small,
        large,
        text,
        disable,
        rounded,
        [classcustom]: classcustom
    });

    if (to) {
        Component = Link;
    } else if (href) {
        Component = "a";
    }

    //Remove event listener when disable button
    if (disable) {
        Object.keys(init).forEach((key: any) => {
            if (key.startsWith("on") && typeof init[key] === "function") {
                delete init[key];
            }
        });
    }

    return (
        <>
            <Component className={classes} {...init}>
                {lefticon && <span className={$('icon')}>{lefticon}</span>}
                <span className={$("title")}>{children}</span>
                {righticon && <span className={$('icon')}>{righticon}</span>}
            </Component>
        </>
    );
}
