import { faCheckCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import classNames from "classnames/bind";
import { FC } from "react";
import { Link } from "react-router-dom";
import { Image } from "~/components/Images";
import { User } from "~/components/Models/User";
import styles from './Account.module.scss';

const $ = classNames.bind(styles);

interface PropTypes {
    user : User;
}

const Account : FC<PropTypes> = ({ user }: PropTypes) =>  {

    return (
        <Link to={`/${user.nickname}`} className={$('wrapper')}>

            <Image
                className={$('avatar')}
                src={user.avatar}
                alt={user.fullName}
            />

            <div className={$('info')}>
                <h4 className={$('name')}>
                    <span>{user.fullName}</span>
                    {user.tick && <FontAwesomeIcon className={$('check-icon')} icon={faCheckCircle} />}
                </h4>
                <span className={$('user-name')}>{user.nickname}</span>
            </div>

        </Link>
    );
}



export default Account;