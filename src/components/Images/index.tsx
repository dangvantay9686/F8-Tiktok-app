import { Ref, forwardRef, useState } from "react";
import images from "~/assets/imgs";

interface ImageProps {
    className: string;
    src: string;
    alt: string;
    fallback?: string;
}
/**
 * fallback: customFallback = images.noImg
 * Nếu fallback không được truyền từ ngoài vào fallback = images.noImg
 * 
 * fallback: customFallback : để tránh trung tên với fallback bên trong hàm
 */
export const Image = forwardRef(({ src, alt, fallback: customFallback = images.noImg, ...props }: ImageProps, ref: Ref<HTMLImageElement>) => {
    
    const [fallback, setFallback] = useState<string>("");

    const handleImgError = () => {
        setFallback(customFallback)
    }

    return (
        <img
            ref={ref}
            src={fallback || src}
            alt={alt}
            {...props}
            onError={handleImgError}
        />
    )
});



