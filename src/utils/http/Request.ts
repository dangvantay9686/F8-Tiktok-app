import axios, { AxiosInstance } from "axios";

export class Request {
    
    request : AxiosInstance;

    constructor() {
        console.log(process.env.REACT_APP_BASE_URL)
        this.request = axios.create({
            baseURL :  process.env.REACT_APP_BASE_URL
        })
    }

    async get(apiPath : any, params: any) : Promise<any> {
        return (await this.request.get(apiPath, {params : params})).data;
    };

    async post(apiPath : any, params: any) : Promise<any> {
        return (await this.request.post(apiPath, {params : params})).data;
    };
    
    async delete(apiPath : any, params: any) : Promise<any> {
        return (await this.request.delete(apiPath, {params : params})).data;
    };

    async update(apiPath : any, params: any) : Promise<any> {
        return (await this.request.put(apiPath, {params : params})).data;
    };
    
}
