import img from './HD-wallpaper-load-buddha-black-white.jpeg';
import { avatar } from './avartar';

const images = {
    logo : require('./logo.svg').default,
    avatar : avatar(),
    noImg : img,
}

export default images;