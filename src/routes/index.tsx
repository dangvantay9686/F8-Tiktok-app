//Layouts :
import config from "~/configs";
import { default as HeaderOnly } from "~/layouts/Header";
import { Following, Home, Live, Profile, Search, Upload } from "~/pages";


const publicRouter = [
    { path: config.routes.home, component: Home },
    { path: config.routes.following, component: Following },
    { path: config.routes.profile, component: Profile },
    { path: config.routes.upload, component: Upload, layout: HeaderOnly },
    { path: config.routes.search, component: Search, layout: null },
    { path: config.routes.live, component: Live},
];

const privateRouter: never[] = [];

export { privateRouter, publicRouter };
