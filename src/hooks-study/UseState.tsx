import React, { useState } from 'react';

const courses = [
    {
        id : 1, name : 'KTML, CSS'
    },
    {
        id : 2 , name : "JavaScript",
    },
    {
        id : 3, name : "Python"
    }
]

function UseState() {
    const [counter, setCounter] = useState(1);
    const [name, setName] = useState('');
    const [checkedId, setCheckedId] = useState<any>();
    const [checkbox, setCheckbox] = useState<Array<number>>([]);

    const handleIncrease = () => {
        setCounter(counter + 1);
    }

    console.log(checkbox);
    const handleCheckbox  = (id : any) => {
        console.log(id);
        setCheckbox((prev : any) => {
            //khi uncheck thì kiểm tra trong mảng checkbox có id không, nếu có thì lọc nó ra
            if(checkbox.includes(id)){
                return checkbox.filter(item => item !== id)
            }else {
                return [...prev, id];
            }
        })
    }

    const handleSubmit = () => {
        console.log({id : checkedId })
    }

    return (
        <>
            UseState : 
            <br />
            
            <div className="App" style={{ padding: 20 }}>
                <h1>{counter}</h1>
                <button onClick={handleIncrease}> increase </button>
            </div>

            <input
                value={name}
                onChange={(e) => setName(e.target.value)} 
            />

            <button onClick={() => setName("Dang Van Tay")}>Click</button>

            <div style={{ padding : 32 }}>
                {courses.map((course) =>  (
                    <div key={course.id}> 
                        <input 
                            type='radio'
                            checked = {checkedId === course.id}
                            onChange={() => setCheckedId(course.id)}
                        />
                        <label>{course.name}</label>
                    </div>
                ))}

                <button onClick={handleSubmit}>Submit</button>
            </div>

            <div style={{ padding : 32 }}>
                {courses.map((course) =>  (
                    <div key={course.id}> 
                        <input 
                            type='checkbox'
                            checked = {checkbox.includes(course.id)}
                            onChange={() => handleCheckbox(course.id)}
                        />
                        <label>{course.name}</label>
                    </div>
                ))}

                <button onClick={handleSubmit}>Submit</button>
            </div>
        </>
    );
}

export default UseState;

/**
 * 1/ Dùng khi nào: 
 * Dữ liệu thay đổi => giao diện được tự động cập nhật
 * 
 */

/**
 * 2/ Lưu ý: 
 * Component được rerender sau khi gọi setState
 * Initial State chỉ dùng cho lần đầu
 * Set-State với callback
 * Initial-State với callback
 */