import React, { useLayoutEffect, useState } from "react";
/**
 * Hoạt động của UseLayoutEffect
 * 1/ Cậpj nhật lại state
 * 2/ Cập nhật DOM (mutated)
 * 3/ Gọi cleanup nếu deps thay đổi (sync)
 * 4/ Gọi useLayoutEffect callback
 * 5/ Render lại UI
 */

/**
 * Hoạt động của UseEffect
 * 1/ Cậpj nhật lại state
 * 2/ Cập nhật DOM (mutated)
 * 3/ Render lại UI
 * 4/ Gọi cleanup nếu deps thay đổi
 * 5/ Gọi UseEffect callback
 */



 
function UseLayoutEffect () {
    const [count, setCount] = useState<number>(0);

    useLayoutEffect(() => {
        console.log(count);
        if(count > 3) {
            setCount(0)
        }
    }, [count])

    const handleRun = () => {
        setCount(count + 1);
    }

    return (
        <>
            UseLayoutEffect  : 
            <br />
            
            <div>
                <h1>{count}</h1>
                <button 
                    onClick={handleRun}
                >RUN</button>
            </div>
        </>
    );
}

export default UseLayoutEffect;