import { useEffect, useState } from "react";
/**
 * Cách sử dụng :
 * 1/ useEffect(callback)
 * - gọi callback mỗi khi component re-render
 * - useEffect sẽ được gọi sau khi component render xong.
 * 2/ useEffect(callback, [])
 * - chỉ gọi callback một lần sai khi component mounted => Thường dùng khi muốn gọi API một lần 
 * 3/ useEffect(callback, [deps])
 * - callback được gọi khi deps thay đổi => Khi A thay đổi dẫn đến B thay đối
 */
//----------------
/**
 * 1/ Trong cả 3 trường hợp callback luôn được gọi sau khi components mounted
 * 2/ Clean-up Function : sẽ được gọi lại trước khi component unmounted
 * 3/ clean-up function: luôn được gọi trước khi callback được gọi (trừ lần component được mounted)
 */

const tabs : Array<string> = ['posts', 'comments', 'albums'];

function UseEffect() {
    const [title, setTitle] = useState<string>('');
    const [posts, setPosts] = useState<Array<any>>([]);
    const [type, setType] = useState<string>('posts');
    const [showGoToTop, setShowGoToTop] = useState<boolean>(false);
    const [width,  setWidth] = useState<number>(window.innerWidth);

    const [countDown, setCountDown] = useState<number>(180);
    const [count, setCount] = useState<number>(1);
    const [avatar,  setAvatar] = useState<any>();

    //2/ useEffect(callback, [])
    // useEffect(() => {
    //     fetch('https://jsonplaceholder.typicode.com/posts')
    //         .then((res: any) => res.json())
    //         .then((posts: Array<Data>) => {
    //             setPosts(posts);
    //         });
    // }, [])

    //3/ useEffect(callback, [deps])
    useEffect(() => {
        fetch(`https://jsonplaceholder.typicode.com/${type}`)
            .then((res: any) => res.json())
            .then((posts: any) => {
                setPosts(posts);
            });
    }, [type]);

    useEffect(() => {
        const handleScroll = () => {
            setShowGoToTop(window.scrollY >= 20)
        }
        window.addEventListener('scroll', handleScroll);
        console.log('add')

        //clean up function :
        return () => {
            window.removeEventListener('scroll', handleScroll)
            console.log('removed...')
        }

    }, []);

    useEffect(() => {
        const handleResize = () => {
            setWidth(window.innerWidth)
        }
        window.addEventListener('resize', handleResize)
    }, [])


    // useEffect(() => {
    //     const timeId = setInterval(() => {
    //         setCountDown((prev)=> prev - 1);
    //         console.log("Countdown : " , countDown)
    //     }, 1000);

    //     return () => clearInterval(timeId);
    // }, [])

    useEffect(() => {
        console.log("Mounted Or Render lan " + count);

        return () => {
            console.log('Clean up lan ' + count)
        }
    })

    useEffect(() => {
        return () => {
            avatar && URL.revokeObjectURL(avatar.preview);
        }
    }, [avatar])
    
    const handlePreviewAvartar = (e : any) => {
        const file = e.target.files[0];
        file.preview = URL.createObjectURL(file);
        setAvatar(file);
    }

    return (
        <>
            UseEffect : 
            <br />

            <div>
                <input
                    type="file"
                    onChange={handlePreviewAvartar}
                />
                {avatar && 
                    <img src={avatar.preview} alt="" width='60%'/>
                }
            </div>
            <div>
                <h1>{ count }</h1>
                <button onClick={() => setCount(count + 1)}>Click Me</button>
            </div>
            {/* <div>
                <h1>{ countDown }</h1>
            </div> */}

            <div>
                <input
                    value={title}
                    onChange={e => setTitle(e.target.value)}
                />
            </div>

            {tabs.map((tab : string) => (
                <button 
                    key={tab} 
                    style={type === tab ? {
                        color : '#fff',
                        backgroundColor : '#333'
                    } : {}}
                    onClick={() => setType(tab)}
                >
                    { tab }
                </button>
            ))}
            <ul>
                {posts.map(post => (
                    <li key={post.id}>{ post.title || post.name}</li>
                ))}
            </ul>

            { showGoToTop &&
                <button
                    style={{
                        position : "fixed",
                        right : 20,
                        bottom : 20

                    }}
                >
                    GO TO TOP
                </button>
            }

            <h1>{width}</h1>
        </>
    );
}

export default UseEffect;

/**
 * UseEffect được sử dungj khi chương trình bị Side Efect : tức là khi một tác động nào đó làm chương trình 
 * thay đổi => thay đổi dữ liệu 
 * 
 * Sử dụng khi nào :
 * 1/ Update DOM 
 * 2/ Call API
 * 3/Listen DOM events : scroll, resize, ...
 * 4/ Clean Up : clear listener, unsubcribe, clear timer
 * 
 * 
 * Kiến thức nền tảng :
 * 1/Events DOM
 * 2/ Observer pattern
 * 3/ Closure Function
 * 4/ Timer : setInterval, setTimeout, ClearINterval, clearTimeout
 * 5/useState
 * 6/ Mounted / Unmounted
 * 7/toán tử ===
 * 8/ Call API
 */