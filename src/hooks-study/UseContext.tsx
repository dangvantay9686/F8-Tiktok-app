import React, { useState , Context, createContext} from "react";
import Content02 from './items/Content02';

/**
 * Context :  khái niệm truyền dữ liệu từ component cha xuống các component con
 *  mà không cần dùng đến props
 *
 */

/** 
 * 1. Tạo Context (cho việc sử dụng dữ liệu )
 * 2. Tạo Provider (Nhà cung cấp dữ liệu)
 * 3. Tạo Consumer (Nhà tiêu thụ dữ liệu)
*/

/**
 * Cách hoạt động : Nếu có các component lồng nhau,
 * và dữ liệu được truyền xuyên suốt các component
 * Nếu tạo context ở component cha
 * => nó sẽ truyền được dữ liệu xuống bất kì conponent con thuộc thằng cha nó
 * 
 * provider : đóng vai trò nhận dữ liệu 
 * 
 * consumer : đóng vai trò sử dụng dữ liệu
 */

export const ThemeContext : Context<any> = createContext('');

function UseContext() {
    const [theme, setTheme] = useState('dark');

    const toggleTheme = () => {
        setTheme(theme === 'dark' ? 'light' : 'dark')
    }

    return (
        <>
            UseContext :
            <br />
            <ThemeContext.Provider value = { theme }>
                <div>
                    <button onClick={toggleTheme}>Toggle Me</button>
                    <Content02/>
                </div>
            </ThemeContext.Provider>
        </>
    );

}

export default UseContext;