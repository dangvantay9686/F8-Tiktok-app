import React, { useCallback, useState } from "react";
import Content01 from "./items/Content01";

/**
 * useCallback :  cách sử dụng như useEfect, khi depences thay đổi thì sau mỗi lần re-render, 
 * thì userCallback sẽ tạo ra call-back mới => return về tham chiếu mới 
 * => nếu không thì nó tham chiếu cái cũ
 * 
 * 
 * Chú ý : 
 * Nếu sử dụng React.memo cho component con để tránh re-render không cần thiết
 * => props ( function )của component con phải sử dụng useCallback để tránh re-render không cần thiết
 * 
 */
function UseCallback() {
    const [count, setCount] = useState(0);

    const handleIncrease = useCallback(() => {
        setCount(prevCount => prevCount + 1);
    }, []);

    return (
        <>
            UseCallback : 
            <br />
            
            <div>
                <Content01 onIncrease={handleIncrease} />
                <h1>{count}</h1>
            </div>
        </>
    )

}

export default UseCallback;
