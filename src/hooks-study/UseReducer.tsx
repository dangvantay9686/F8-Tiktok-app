import { useReducer, useRef } from "react";
/**
 * useReducer  có cách sử dụng giống useState
 * 
 * Khác nhau :
 * useState phù hợp với các kiểu dữ liệu đơn giản, hoặc array, object có 1 cấp không phức tạp
 * useReducer : nếu kiểu dữ liệu phức tạp hơn, nhiều tầng, nhiều lớp lồng nhau 
 * 
 */

/**
 * useState : 
 * 1/ Xác định giá trị khởi tao
 * 2/ Xác định actions sau khi khỏi tạo (cụ thể : UP/ DOWN)
 */

/**
 * useReducer : 
 * 1/ Xác định giá trị khởi tao : init state
 * 2/ Xác định actions sau khi khỏi tạo (cụ thể : UP/ DOWN)
 * 3/ tạo hàm reducer
 * 4/ Dispatch (kích hoạt một action)
 */
const initState = 0;

enum Action {UP, DOWN}

const reducer = ((state : number, action : Action) => {
    console.log("reducer running ...")
    switch(action) {
        case Action.UP:
            return state + 1;
        case Action.DOWN:
            return state - 1;
        default :
            throw new Error('Invalid action.')
    }
})

function UseReducer () {

    const [count, dispatch] = useReducer(reducer, initState);

    return (

        <>
            UseReducer :
            <br />
            
            <JobsUseReducer />
            <br />

            Reducer :
            <br/>
            <h1>{ count }</h1>

            <button
                onClick={() => dispatch(Action.DOWN)}
            >Down
            </button>

            <button
                onClick={() => dispatch(Action.UP)}
            >Up
            </button>
        </>
    )

}
export default UseReducer;

class JobState {
    job !: string;
    jobList !: Array<string>
    constructor(job : string, jobList : Array<string>) {
        Object.assign(this, {job, jobList});
    }
}

const jobInitState = new JobState ("",  [])

enum JobStatus { SET, ADD, DELETE }

interface JobAction {
    value : string;
    status : JobStatus;
}

const set = (value : any, status : JobStatus) : JobAction => {
    return {
        value : value,
        status : status
    }
};

const jobReducer = ((state : JobState, action : JobAction) => {
    switch(action.status) {
        case JobStatus.SET:
            return new JobState(action.value , [...state.jobList])
        
        case JobStatus.ADD:
            return new JobState(action.value, [...state.jobList, action.value])
        
        case JobStatus.DELETE:
            const removedJob = [...state.jobList];

            removedJob.splice(Number(action.value), 1);

            return new JobState('', removedJob);

        default:
            throw new Error('Invalid status ...')
    }
});


const JobsUseReducer = ()  => {
    const [jobState, dispatch] = useReducer(jobReducer, jobInitState);
    
    const {job , jobList } = jobState;

    const inputRef = useRef<any>();

    const handleSubmit = (e : any) => {
        dispatch(set(job, JobStatus.ADD));

        dispatch(set('', JobStatus.SET));

        console.log(inputRef.current)

        inputRef.current.focus();
    }

    console.log('jobState : ', jobState);
    
    return (
        <>
            Jobs UseReducer: 
            <h3>Todo List :</h3>
            <br />

            <input 
                type="text" 
                ref = { inputRef }
                value={ job }
                onChange={(e) => dispatch(set(e.target.value, JobStatus.SET))}
                placeholder="Enter job ..."
            />
            <br />

            <button
                onClick={handleSubmit}
            >
                ADD
            </button>
            <br />

            <ul>
                {jobList.map((job, i)=> (
                    <li key={i}>
                        { job }
                        <span onClick={() => dispatch(set(String(i), JobStatus.DELETE))}>
                            &times;
                        </span>
                    </li>
                ))}
            </ul>
        </>
    );
}