import React, { useState } from "react";
import Content from "./items/Content";
/**
 * 1/ memo() => Higher order component (HOC)
 * 2/ useCallback()
 * 
 * memo : ghi nhớ những props của component 
 * => quyết định có render lại đó hay không => tối ưu hiệu năng
 * => Tóm lại : memo sẽ xử lí một component tránh re-render ở những tình huống không cần thiết
 */
function ReactMemo () {
    const [count, setCount] = useState(0);

    const handleIncrease = () => {
        setCount(count + 1)
    }

    return (
        <>
            React.memo : 
            <br />
            <div>
                <Content />
                <h1>{ count }</h1>
                <button
                    onClick={handleIncrease}
                >
                    Increase
                </button>
            </div>
        </>
    )

}

export default ReactMemo;
