import { useMemo, useState } from "react";

/**
 * useMemo :  tránh thực hiện lại những logic không cần thiết
 */

class Product {
    name !: string;
    price !: number;
    constructor(name: string, price: number) {
        Object.assign(this, { name, price });
    }
}
function UseMemo() {

    const [name, setName] = useState<string>('');
    const [price, setPrice] = useState<string>('');
    const [products, setProducts] = useState<Array<Product>>([]);


    const handleSubmit = () => {
        setProducts([...products, new Product(name, Number(price))]);
    }

    const total: number = useMemo(() => {
        return products.reduce((result: number, item: Product) => {
            console.log("render lai ...")
            return result + item.price;
        }, 0);

    }, [products])


    return (
        <>
            <div style={{ padding: '10px 30px' }}>
                useMemo :
                <br />
                <input
                    type="text"
                    value={name}
                    placeholder="Enter name ..."
                    onChange={e => setName(e.target.value)}
                />
                <br />

                <input
                    type="text"
                    value={price}
                    placeholder="Enter price ..."
                    onChange={e => setPrice(e.target.value)}
                />
                <br />

                <button onClick={handleSubmit}>Add</button>
                <br />

                Total : {total}
                <ul>
                    {products.map((item: any, index) => (
                        <li key={index}> {item.name} - {item.price}</li>
                    ))}
                </ul>

            </div>
        </>
    );

}


export default UseMemo;