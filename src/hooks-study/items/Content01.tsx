import { memo } from "react";

/**
 * Memo nhận vào một component và check Props của component có thay đổi hay không
 * nếu ít nhất một prop thay đổi thì component sẽ re-render
 * 
 * memo xử dụng toán tử === để so sánh props có thay đổi hay không
 */
interface Props {
    onIncrease: () => any;
}

function Content01({ onIncrease } : Props) {
    console.log("content re-render use-callback")
    return (
        <>
            <div>HELLO Use Callback</div>

            <button
                onClick={onIncrease}
            >
                Increase
            </button>
        </>
    );
}

export default memo(Content01);