import React, { FC, useContext } from "react";

import { ThemeContext } from "../UseContext";

function Content02  () {
    return (
        <>
            <Paragraph />
        </>
    )
}

export default Content02;

export const Paragraph : FC<any> = () => {

    const theme = useContext(ThemeContext);

    console.log("theme : " , theme)
    return (
        <>
            <h1 className={ theme }>It's Me :))</h1>
        </>
    )
}