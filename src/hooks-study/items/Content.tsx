import { memo } from "react";

/**
 * Memo nhận vào một component và check Props của component có thay đổi hay không
 * nếu ít nhất một prop thay đổi thì component sẽ re-render
 * 
 * memo xử dụng toán tử === để so sánh props có thay đổi hay không
 */

function Content01 () {
    //console.log("content re-render")
    return (
        <>
            <div>HELLO </div>
        </>
    );
}

export default memo(Content01);