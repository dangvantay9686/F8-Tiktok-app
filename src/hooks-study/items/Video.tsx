import { forwardRef, useImperativeHandle, useRef } from "react";
const videoPath = require('../../assets/videos/Download.mp4');

function Video (props : any, ref : any) {

    const videoRef = useRef<any>();

    useImperativeHandle(ref, () => ({
        play () {
            videoRef.current.play();
        },

        pause () {
            videoRef.current.pause();
        }
    }))

    return (
        <>
            <div >
                <video  
                    ref = { videoRef }
                    src={videoPath} 
                    width={250}
                />
            </div>
        </>
    )
}

export default forwardRef(Video);
