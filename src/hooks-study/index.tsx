import { useState } from "react";
import FakeChatApp from "./FakeChatApp";
import ReactMemo from "./ReactMemo";
import UseCallback from "./UseCallback";
import UseContext from "./UseContext";
import UseEffect from "./UseEffect";
import UseImperativeHandle from "./UseImperativeHandle";
import UseLayoutEffect from "./UseLayoutEffect";
import UseMemo from "./UseMemo";
import UseReducer from "./UseReducer";
import UseRef from "./UseRef";


function Hooks () {

    const [show, setShow] = useState(false);

  return (
    <>
      <div style={{ padding: 30 }}>
        <UseImperativeHandle />
      </div>

      <div style={{ padding: 30 }}>
        <UseContext />
      </div>

      <div style={{ padding: 30 }}>
        <UseReducer/>
      </div>

      <div style={{ padding: 30 }}>
        <UseMemo/>
      </div>

      <div style={{ padding: 30 }}>
        <UseCallback />
      </div>

      <div style={{ padding: 30 }}>
        <ReactMemo />
      </div>

      <div style={{ padding: 30 }}>
        <UseRef />
      </div>

      <div style={{ padding: 30 }}>
        <UseLayoutEffect />
      </div>

      <div>
        <FakeChatApp />
      </div>

      <div style={{ padding: 30 }}>

        <button onClick={() => setShow(!show)}>Toggle</button>

        {show && <UseEffect />}

      </div>
    </>
  );

}

export default Hooks;