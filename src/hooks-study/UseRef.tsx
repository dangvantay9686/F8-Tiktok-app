import { useRef, useState } from "react";

/**
 * useRef : 
 * Lưu các giá trị qua tham chiếu ra bên ngoài Function components
 */


function UseRef () {
    const [count, setCount] = useState<number>(60);

    const timerId = useRef<any>(99);

    const handleStart = () => {
        timerId.current = setInterval(() => {
            setCount((prev) => prev - 1)
        }, 1000);
    }

    const handleEnd = () => {
        clearInterval(timerId.current)
    }

    return (
        <>
            UseRef : 
            <br />
            
            <h1>{ count }</h1>
            <button onClick={handleStart}>Start</button>
            <button onClick={handleEnd}>End</button>

        </>
    );
}

export default UseRef;