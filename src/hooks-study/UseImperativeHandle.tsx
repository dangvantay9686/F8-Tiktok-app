import { useEffect, useRef } from "react";
import Video from "./items/Video";

/**
 * Hook này  sinh ra dựa trên yêu cầu tính đóng gói của dữ liệu.
 * Tức là chỉ public ra ngoài Function Component các tính năng cần được xử dụng thôi, 
 * để không tác động nhiều vào component đang có. 
 * => Hạn chế mấy đứa DEV phá cái component , làm cho nó hoạt động không hiệu quả
 */

function UseImperativeHandle () {

    const videoRef = useRef<any>();

    useEffect(() => {
        console.log(videoRef.current)
    }, [])

    const handlePlay = () => {
        videoRef.current.play();
    }

    const handlePause = () => {
        videoRef.current.pause();
    }

    return (
        <>
            UseImperativeHandle : 
            <br />
            <Video  ref ={ videoRef }/>
            <br />
            <button onClick={handlePlay}>Play</button>
            <button onClick={handlePause}>Pause</button>

        </>
    )
}

export default UseImperativeHandle;