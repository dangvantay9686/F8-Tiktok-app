import { Fragment } from 'react';
import { Route, BrowserRouter as Router, Routes } from 'react-router-dom';

import DefaultLayout from "~/layouts/DefaultLayout";
import { publicRouter } from "~/routes";

export default function App() {
    return (
        <>
            <Router>
                <Routes>
                    {publicRouter.map((route: any, index: number) => {
                        
                        const Page: any = route.component;

                        let Layout : any = DefaultLayout;

                        if(route.Layout) {
                            Layout = route.layout;
                        } else if (route.layout === null){
                            Layout = Fragment;
                        }

                        return (
                            <Route key={index} path={route.path} element={
                                <Layout>
                                    <Page />
                                </Layout>
                            }/>
                        )
                    })}
                </Routes>
            </Router>
        </>
    )
}


